
public class SumEvenFibonacci {
	
	private static int sum = 0;
	
	//initialize the  first terms
	private static int previous = 1;

	private static int position = 2;
	
	//next will be used in changing the current position
	private static int next;
	
	public static void main(String[] args)
	{
		//position is the current fibonacci number
		while (position < 400)
		{
			//if the current fibonacci number is even then add it to the sum
			if (position%2 == 0)
				sum += position;
			
			//the next number is the current plus the previous
			next = position + previous;
			
			//update the previous to be the current and the current to be the next
			previous = position;
			position = next;
			
		}
		
		System.out.println("Sum of all the even-valued terms below 400: "+sum);
	}
}
