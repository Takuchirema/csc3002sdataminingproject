
public class SumSquare {
	
	//initialize the values
	private static int sum = 0;
	private static int sumSquares = 0;
	
	public static void main (String[] args)
	{
		for (int i=1;i<151;i++)
		{
			//increment the sum by the natural number
			sum += i;
			
			//increment the sumSquare by the square of the natural number
			sumSquares += (i*i);
			
		}
		
		System.out.println("The difference: " + (sum*sum - sumSquares));
	}
}
