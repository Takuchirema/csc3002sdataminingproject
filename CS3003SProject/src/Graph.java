import java.util.ArrayList;


public class Graph {

	private ArrayList<CrimeNode> crimeNodes = new ArrayList<CrimeNode>();
	
	public ArrayList<int[]> edges = new ArrayList<int[]>();
	
	private ArrayList<ArrayList<Double> > distances = new ArrayList<ArrayList<Double> >(); ;
	
	public Graph(ArrayList<CrimeNode> crimeNodes,ArrayList<ArrayList<Double> > distances){
		
		this.crimeNodes = crimeNodes;
		this.distances = distances;
		
	}
	
	public Graph(){
		
	}
	
	public ArrayList<CrimeNode> getCrimeNodes(){
		return crimeNodes;
	}
	
	public ArrayList<ArrayList<Double> > getDistances(){
		return distances;
	}
}
