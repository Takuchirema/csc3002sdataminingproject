import java.util.ArrayList;


public class CutGraph {
	
	private double mincut;
	
	private ArrayList<CrimeNode> subgraph = new ArrayList<CrimeNode>();
	
	public CutGraph(ArrayList<CrimeNode> masternodes,double mincut){
		this.mincut = mincut;
		this.subgraph = masternodes;
	}
	
	public CutGraph(){
		
	}
	
	public double getMinCut(){
		return mincut;
	}
	
	
	public ArrayList<CrimeNode> getSubGraph(){
		return subgraph;
	}

}
