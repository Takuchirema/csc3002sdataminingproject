import java.sql.*;
import java.sql.Connection.*;
import java.sql.DriverManager.*;
import java.sql.SQLException.*;
import java.util.ArrayList;
 
public class CrimeCollection
{
	 
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL ="jdbc:mysql://127.0.0.1:3306/crimedata";
	static final String DB_URL2 ="jdbc:mysql://127.0.0.1:3306/cluster";
	static final String username="tebogo";
	static final String paswd="password";
	static final String table = "table1";
	static final int columNo = 12;
	static Statement stat = null;
	static Connection con =null;
	String sql = "";
	static ResultSet crimes =null;
	public static Graph graph;
	public static ArrayList<CrimeNode> CrimeList;
	public static ArrayList<CrimeNode> GraphCrimes =null;
	static Statement statement = null;
	static Connection connect =null;
	static String sQl ;
	static ResultSet graphTable;
    
	public void createGraph()
	{
	
	try{

		Class.forName(JDBC_DRIVER);
		
		System.out.println("Connecting to the Database");
		con = DriverManager.getConnection(DB_URL,username,paswd);
		
		System.out.println("Successfully Connected");
		stat = con.createStatement();
		String sqlQuery = "SELECT * FROM crime_table";
		System.out.println(stat.executeQuery(sqlQuery));
		
		crimes =stat.executeQuery(sqlQuery);
		int CrimeNodeID = 0;
		
		 CrimeList = new ArrayList<>();
		
		
		while (crimes.next())
		{
			CrimeNodeID ++;
			CrimeNode crime = new CrimeNode(CrimeNodeID);
			
			//Crimenode.incident_day = crimes.getString("INCIDENT_DAY");
			crime.incident_day = crimes.getString("INCIDENT_DAY");
			crime.incident_time = crimes.getString ("INCIDENT_TIME");
			crime.victim = crimes.getString ("VICTIM");
			crime.suspect = crimes.getString ("SUSPECT");
			crime.victim_age = crimes.getString ("VICTIM_AGE");
			crime.suspect_age = crimes.getString ("SUSPECT_AGE");
			crime.method_capture = crimes.getString ("METHOD_VICTIM_CAPTURE");
			crime.substance_abuse = crimes.getString ("SUBSTANCE_ABUSE_SUSPECTED");
			crime.suspect_disguised = crimes.getString ("SUSPECT_DISGUISED");
			crime.incident_loc = crimes.getString ("INCIDENT_LOC");
			
			CrimeList.add(crime);
			
			//System.out.println(incident_day+ " " + incident_time + " " + victim +" "+ suspect+ " " + victim_age + " " + suspect_age + " "+ method_capture + " " + substance_abuse + " " + suspect_disguised + " "+ incident_loc);
		 }
		
		graph = new Graph(CrimeList, null);
		
	   }
	   catch(Exception e){System.out.println(e);}

	}
	
	public ArrayList<CrimeNode> getCrimeNodes()
	{
		return CrimeList;
	}
	
	public Graph getGraph()
	{
		return graph;
	}
	
	public static void saveGraph(ArrayList<Graph> graphs) throws SQLException, ClassNotFoundException
	{ 
		Class.forName(JDBC_DRIVER);
		ArrayList<Graph> gr = graphs;
		
		System.out.println("Connecting to the Database");
		connect = DriverManager.getConnection(DB_URL2,username,paswd);
		
		System.out.println("Successfully Connected");
		statement = connect.createStatement();
		
	    int i =0;
	    Graph graph1;
	    CrimeNode crime;
	    
	    dropTables(statement);
	    
	    while (i < gr.size())
	    {
	    	 Graph cluster = gr.get(i);
	    	 String graphName = "graph"+i;
	    	 String sql = "CREATE TABLE "+graphName+
	                   "( INCIDENT_DAY VARCHAR(255), " +
	                   " INCIDENT_TIME VARCHAR(255), " + 
	                   " VICTIM VARCHAR(255), " + 
	                   " SUSPECT VARCHAR(255), " + 
	                   " METHOD_VICTIM_CAPTURE VARCHAR(255), " + 
	                   " SUBSTANCE_ABUSE_SUSPECTED VARCHAR(255), " + 
	                   " SUSPECT_DISGUISED VARCHAR(255), " + 
	                   " SUSPECT_AGE VARCHAR(255), " + 
	                   " INCIDENT_LOC VARCHAR(255), " + 
	                   " VICTIM_AGE VARCHAR(255) ) " ; 
	    	 
	    	 //Assuming that a graph is an arraylist
	    	 
	    	 //For each CrimeNode in the graph;
	    	 	//System.out.println(statement.executeUpdate(sql));
	    	 
	    	 	statement.executeUpdate(sql);
	    	 	
	    	   	ArrayList<CrimeNode> ClusterCrimes = cluster.getCrimeNodes();
	    	   	
	    	    sQl = "SELECT * FROM "+graphName;
	    	   	
	    		//System.out.println(statement.executeQuery(sQl));
	    	    statement.executeQuery(sQl);
	    	    
	    		graphTable =statement.executeQuery(sQl);
	    	   	
	    	   	for (int j=0; j< ClusterCrimes.size(); j++)
	    	   	{
		    	   	CrimeNode crime1 = ClusterCrimes.get(j);
		    	   	
		    	    String data = "INSERT INTO "+graphName+"( INCIDENT_DAY ,INCIDENT_TIME ,VICTIM," +
		    	    		" SUSPECT , METHOD_VICTIM_CAPTURE ,SUBSTANCE_ABUSE_SUSPECTED ," +
		    	    		"SUSPECT_DISGUISED ,SUSPECT_AGE ,INCIDENT_LOC ," +
		    	    		"VICTIM_AGE ) VALUES ( '"+crime1.incident_day+"', '"+
			                   crime1.incident_time+"', '"+
			                   crime1.victim+"', '"+ 
			                   crime1.suspect+"', '"+ 
			                   crime1.method_capture+"', '"+ 
			                   crime1.substance_abuse+"', '"+ 
			                   crime1.suspect_disguised+"', '"+ 
			                   crime1.suspect_age+"', '"+ 
			                   crime1.incident_loc +"', '"+ 
			                   crime1.victim_age+"' )"; 
		    	    //System.out.println(data);
		    	    
		    	    //System.out.println(statement.executeUpdate(data));
		    	    statement.executeUpdate(data);
	    	   	}
	    	   	
	    	   	i++;
	    }
	    
		//takes an arraylist of HCS graphs
		//For each graph, get the Arraylist of CrimeNodes and create tables
		//Save tables on remote database
	}
	
	public static void dropTables(Statement statement){
		System.out.println("Started deleting tables ...");
		int i = 0;
		loop:
		while (true){
			
			try{
				String sql = "DROP TABLE graph"+i;
				statement.executeUpdate(sql);
			}catch(Exception e){
				break loop;
			}
			i++;
		}
		
		System.out.println("Finished deleting tables ...");
		
	}
}