import java.util.ArrayList;
import java.util.HashMap;


public class Cluster {
	
	private ArrayList<CrimeNode> crimes;
	public String clusterName;
	
	//create html to be shown in browser for cluster
	private String html = "<tr class=\"topRow\"><td><h1>Day</h1></td><td><h1>Time</h1></td><td><h1>Victim</h1></td>" +
			"<td><h1>Suspect</h1></td><td><h1>VAge</h1></td><td><h1>SAge</h1></td>" +
			"<td><h1>SAbuse</h1></td><td><h1>Disguise</h1></td></tr>";
	
	//is a list of all the locations this group committed crimes in and the numbers
	//key is the location and the list crimes committed in there
	private HashMap<String,ArrayList<CrimeNode>> crimesCommitted = new HashMap<String,ArrayList<CrimeNode>>() ;
	
	public Cluster(ArrayList<CrimeNode> crimes,String clusterName){
		this.crimes = crimes;
		this.clusterName = clusterName;
	}
	
	public HashMap<String,ArrayList<CrimeNode>> createStatistics(){
		
		for (int i=0;i<crimes.size();i++){
			CrimeNode crime = crimes.get(i);
			
			String location = crime.incident_loc;
			if (crimesCommitted.containsKey(location)){ //check if the location is already represented
				crimesCommitted.get(location).add(crimes.get(i));
			}else{
				ArrayList<CrimeNode> loc = new ArrayList<CrimeNode>();
				loc.add(crimes.get(i));
				crimesCommitted.put(location,loc);
			}
			
			html = html+"<tr class=\"cluster-"+location+"\"><td><h1>"+crime.incident_day+"</h1></td><td><h1>"+
					crime.incident_time+"</h1></td><td><h1>"+crime.victim+"</h1></td>" +
					"<td><h1>"+crime.suspect+"</h1></td><td><h1>"+crime.victim_age+"</h1></td><td><h1>"+
					crime.suspect_age+"</h1></td><td><h1>"+crime.substance_abuse+"</h1></td><td><h1>"+
					crime.suspect_disguised+"</h1></td></tr>";
			
		}
		
		html = "<table class=\"location-cluster\" id=\"cluster-name\"><tr class=\"gang-name\"><td><h1>"+clusterName+"</h1><td>" +
				"</tr>"+html+"</table>";
		
		return crimesCommitted;
	}
	
	public String getHTML(){
		return html;
	}
	
	public ArrayList<CrimeNode> getCrimes(){
		return crimes;
	}
	
	public int getTotalCrimes(){
		return crimes.size();
	}
	
	public ArrayList<CrimeNode> getLocCrimes(String location){
		return crimesCommitted.get(location);
	}

}
