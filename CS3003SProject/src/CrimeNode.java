import java.util.ArrayList;
import java.util.Arrays;


public class CrimeNode {
	
	public int id;
	public String incident_day;
	public String incident_time;
	public String victim;
	public String suspect;
	public String victim_age;
	public String suspect_age;
	public String method_capture;
	public String substance_abuse;
	public String suspect_disguised;
	public String incident_loc;
	
	public int connected =0;
	
	public ArrayList<CrimeNode> MasterNode = new ArrayList<CrimeNode>(Arrays.asList(this));
	
	public CrimeNode(int id){
		this.id = id;
	}
	
	public int getId()
	{
		return id;
	}
	

}
