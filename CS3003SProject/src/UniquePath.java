import java.util.ArrayList;


public class UniquePath {
	
	private ArrayList<Integer> path = new ArrayList<Integer>();
	
	private int[] bridge;
	
	public UniquePath(ArrayList<Integer> path,int[] bridge){
		
		this.path = path;
		this.bridge = bridge;
	}
	
	public UniquePath(ArrayList<Integer> path){
		
		int size = path.size();
		
		this.path = path;
		//this.bridge = new int[]{path.get(size - 2),path.get(size -1)} ;
		this.bridge = new int[]{path.get(0),path.get(1)} ;
	}
	
	public ArrayList<Integer> getPath(){
		return path;
	}
	
	public int[] getBridge(){
		return bridge;
	}
	
	public void setBridge(int[] newbridge){
		
		//int bridgePosition = path.indexOf(bridge[0]);
		
		//int newbridgePosition = path.indexOf(newbridge[0]);
		
		//if (newbridgePosition > bridgePosition){
			this.bridge = newbridge;
			System.out.println(" new bridge "+(newbridge[0]+1)+"-"+(newbridge[1]+ 1) );
		//}
		
	}
	
	public ArrayList<Integer>  setPath(ArrayList<Integer> newpath,int[] newbridge){
		
		ArrayList<Integer> newList = new ArrayList<Integer>();
		
		int bridgePosition = path.indexOf(newbridge[0]);
		
		int newbridgePosition = newpath.indexOf(newbridge[0]);
		
		if (newbridgePosition < bridgePosition){
			
			newList = new ArrayList<Integer>(newpath);
			newList.addAll(path.subList(bridgePosition + 1,path.size() ));
			
			path = newList;
		}
		
		return newList;
	}
	
}
