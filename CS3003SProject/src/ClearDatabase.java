import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ClearDatabase {
  public void clearData() {

        Connection conn = null;

        try {
      String drName = "com.mysql.jdbc.Driver";              // Load the MySQLJDBC driver

      Class.forName(drName);

      String url = "jdbc:mysql://127.0.0.1:3306/demodb";       // Create a conn to the database
      String username = "root";
      String password = "";

      conn = DriverManager.getConnection(url, username, password);
      System.out.println("Database connection successful!");
        } catch (ClassNotFoundException e) {
      System.out.println("Driver not found " + e.getMessage());

        } catch (SQLException e) {
      System.out.println("Connection failed " + e.getMessage());
        }
        try {
      Statement statement = conn.createStatement();


      statement.executeUpdate("TRUNCATE Data");
      System.out.println("Successfully truncated Data");
        } catch (SQLException e) {
      System.out.println("Could not truncate test_table " + e.getMessage());
        }
  }
}
