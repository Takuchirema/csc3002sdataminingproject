/*
**Distance Matrix class to create the graphs to be used in the HCS implementation.
**This class creates the edges between crime nodes, based on the following attributes:
** -Location longitude and latitude coordinates; the day of the crime; and the time it happened
**
**Tinashe Madzingaidzo, MDZTIN003
*/

import java.util.*;
import java.io.*;
import java.lang.*;
import java.text.*;

public class DistanceMatrix
{
    private static HashMap<String, String> coordMap = new HashMap<>();
    private static ArrayList<int[]> edgeConnectivity = new ArrayList<int[]>();
    private static HashMap<String, Double> locationDist = new HashMap<>();
    private static ArrayList<CrimeNode> c = new ArrayList<>();
    private static Graph graph = new Graph();
    
    public static void getData()
    {
        ArrayList<String> crime_List = new ArrayList<>();
        
        coordMap.put("Cape Town Central","-33.92427 18.41870");
        coordMap.put("Woodstock","-33.92830 18.44071");
        coordMap.put("Sea Point","-33.91695 18.38755");
        coordMap.put("Rondebosch","-33.96579 18.48102");
        coordMap.put("Maitland","-33.92097 18.50663");
        coordMap.put("Pinelands","-33.94100 18.49704");
        coordMap.put("Claremont","-33.98600 18.47212");
        coordMap.put("Mowbray","-33.95003 18.49586");
        coordMap.put("Athlone","-33.96526 18.50179");
        coordMap.put("Lansdowne","-33.98567 18.50031");
        coordMap.put("Langa","-33.94451 18.53148");
        coordMap.put("Wynberg","-34.00845 18.46618");
        coordMap.put("Milnerton","-33.86597 18.53444");
        coordMap.put("Manenberg","-33.98387 18.55522");
        coordMap.put("Dieprivier","-34.03423 18.46470");
        coordMap.put("Goodwood","-33.91167 18.55225");
        coordMap.put("Gugulethu","-33.98207 18.57971");
        coordMap.put("Bishop Lavis","-33.94789 18.57897");
        coordMap.put("Philippi Central","-33.98449 18.51428");
        coordMap.put("Hout Bay","-34.02087 18.36826");
        coordMap.put("Elsies River","-33.93571 18.58357");
        coordMap.put("Nyanga","-33.99538 18.58491");
        coordMap.put("Parow","-33.90679 18.58081");
        coordMap.put("Kirstenhof","-34.06945 18.45431");
        coordMap.put("Grassy Park","-34.06339 18.51070");
        coordMap.put("Steenberg","-34.07364 18.47063");
        coordMap.put("Muizenberg","-34.08988 18.49586");
        coordMap.put("Phillippi East","-34.03450 18.55819");
        coordMap.put("Mitchells Plain","-34.04859 18.60569");
        coordMap.put("Delft","-33.96144 18.64725");
        coordMap.put("Mfuleni","-34.00648 18.68585");
        coordMap.put("Khayelitsha","-34.03744 18.67695");
        coordMap.put("Lingelethu-West","-32.69452 26.30576");
        coordMap.put("Kraaifontein","-33.85342 18.71852");
        coordMap.put("Harare","-34.05829 18.67249");
        coordMap.put("Fish Hoek","-34.13409 18.41870");
        coordMap.put("NoordHoek","-34.09475 18.39497");
        coordMap.put("Strand","-34.11232 18.84921");
        coordMap.put("Atlantis","-33.50627 18.48696");
        
         CrimeCollection crimeData = new CrimeCollection();
        crimeData.createGraph();
        graph = crimeData.getGraph();
        c = crimeData.getCrimeNodes();
       
        graph = new Graph(c,calcDistances());
        
        graph.edges = edgeConnectivity;
    }
    public static HashMap<String,String> getLocations()
    {
    	return coordMap;
    }
    
    public static Graph getGraph(){
    	
    	return graph;
    }
    public static ArrayList<ArrayList<Double>> calcDistances()
    {
        ArrayList<double[]> weights = new ArrayList<>();   //arraylist of arrays of weights to be used
        ArrayList<String[]> crimes = new ArrayList<>();    //arraylist of the crimes' useful attributes
        String numbers = "";
        double dist = 0;
        
        int size = c.size();
        
        ArrayList<ArrayList<Double> > edges = new ArrayList<ArrayList<Double> >();
        String day = "";
        String time = "";
        String location = "";
		
        for(CrimeNode entry:c)
        {
            day = entry.incident_day;
            time = entry.incident_time;
            location = entry.incident_loc;
            crimes.add(new String[]{day,time,location});
            time = "";
            day = "";
            location = "";
        }
        
        for(String[] item:crimes)
        {
            numbers = coordMap.get(item[2]);    
            String[] coords = numbers.split(" ");//get coordinates from hashmap
            double c1 = Double.parseDouble(coords[0]);
            double c2 = Double.parseDouble(coords[1]);
            double weightedVars[] = new double[6];
            weightedVars[0] = c1;            //lattitude
            weightedVars[1] = c2;     
            switch(item[0])
            {
                case "Monday":
                    weightedVars[2]= 0;
                    weightedVars[3]= 25;
                    break;
                case "Tuesday":
                    weightedVars[2]= 19.55;
                    weightedVars[3]= 15.59;
                    break;
                case "Wednesday":
                    weightedVars[2]= 24.38;
                    weightedVars[3]= 5.56;
                    break;
                case "Thursday":
                    weightedVars[2]= 10.85;
                    weightedVars[3]= -22.53;
                    break;
                case "Friday":
                    weightedVars[2]= -10.85;
                    weightedVars[3]= -22.53;
                    break;
                case "Saturday":
                    weightedVars[2]= -24.38;
                    weightedVars[3]= -5.56;
                    break;
                case "Sunday":
                    weightedVars[2]= -19.55;
                    weightedVars[3]= 15.59;
                    break;
            }
            switch(item[1])                            //weighted days
            {
                case "morning":                           //weighted times
                    weightedVars[4]= 0;
                    weightedVars[5]= 25;
                    break;
                case "noon":
                    weightedVars[4]= 25;
                    weightedVars[5]= 0;
                    break;
                case "evening":
                    weightedVars[4]= 0;
                    weightedVars[5]= -25;
                    break;
                case "night":
                    weightedVars[4]= -25;
                    weightedVars[5]= 0;
                    break;
            }
            weights.add(weightedVars);    
        }
       
        double diff=0;
        int count=0;
        double totRow=0;
        double meanRow=0;
        double stdv=0;
        double sd=0;
        
        
          for(count =0;count<6;count++)
          {
              for(int i=0;i<weights.size();i++)
              {
                  totRow+=weights.get(i)[count];
              }
              meanRow=totRow/weights.size();
              for(int j=0;j<weights.size();j++)
              {
                  diff+=Math.pow((weights.get(j)[count] - meanRow),2);
              }
              stdv=Math.sqrt(diff/(weights.size()-1));
              for(int k=0;k<weights.size();k++)
              {
                  sd=(weights.get(k)[count]-meanRow)/stdv;
                  
                  if (count == 0 || count == 1)
                      sd = sd*100;
                  
                  weights.get(k)[count]=sd;
              }
              totRow=0;
              diff=0;
          }
          
          
          double dif=0;
          double diffSqre=0;
          double diffTot = 0;
          double answer = 0;
          double sum=0;
          
          for(int l=0;l<weights.size();l++)
          {
        	  ArrayList<Double> node = new ArrayList<Double>();
          	  edges.add(node);
          	  
              for(int m=0;m<weights.size();m++)
              {
                  for(count=0;count<6;count++)
                  {
                      dif= weights.get(l)[count] - weights.get(m)[count];//Calculate distance between
                      diffSqre = Math.pow(dif, 2);                        //crime nodes
                      diffTot+=diffSqre;
                  }
                  answer = Math.sqrt(diffTot);
                  if(0.0<answer && answer>5)
                {
                	
                     edges.get(l).add(m, -1.0);
                    
                    edgeConnectivity.add(new int[]{l,m});

                }else{ 
                
                	if (answer ==0){
                		edges.get(l).add(m, 0.0);
                	}
                	else{
                		edges.get(l).add(m, 1.0);
                	}
                }
                  diffTot=0;
              }
          }
        
        return edges;
    }
    
  
}
